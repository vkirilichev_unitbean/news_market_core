package com.ub.news_market_core.view.search;

import com.ub.core.base.view.search.SearchResponse;
import com.ub.news_market_core.model.ArticleDocument;

import java.util.List;

public class SearchBlogResponse extends SearchResponse {

    private List<ArticleDocument> result;

    public SearchBlogResponse() {
    }

    public SearchBlogResponse(Integer currentPage, Integer pageSize, List<ArticleDocument> result) {
        this.pageSize = pageSize;
        this.currentPage = currentPage;
        this.result = result;
    }

    public List<ArticleDocument> getResult() {
        return result;
    }

    public void setResult(List<ArticleDocument> result) {
        this.result = result;
    }
}
