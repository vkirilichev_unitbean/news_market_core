package com.ub.news_market_core.service;

import com.ub.news_market_core.model.ArticleDocument;
import com.ub.news_market_core.repository.ArticleRepository;
import com.ub.news_market_core.repository.ArticleRepositoryImp;
import com.ub.news_market_core.view.search.SearchBlogRequest;
import com.ub.news_market_core.view.search.SearchBlogResponse;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    ArticleRepository articleRepository;

    @Override
    public List<ArticleDocument> listArticles() {
        return articleRepository.getArticleList();
    }

    @Override
    public void addArticle(ArticleDocument article) {
        articleRepository.addArticle(article);
    }

    @Override
    public void updateArticle(ArticleDocument article) {
        articleRepository.updateArticle(article);
    }

    @Override
    public void deleteArticle(ObjectId id) {
        articleRepository.deleteArticle(id);
    }

    @Override
    public ArticleDocument findArticleById(ObjectId id) {
        return articleRepository.findArticleById(id);
    }

    @Override
    public SearchBlogResponse findAll(SearchBlogRequest request){
        return  articleRepository.findAll(request);
    }

    @Override
    public Long countArticles() {
        return articleRepository.countArticles();
    }
}
