package com.ub.news_market_core.service;

import com.ub.news_market_core.model.ArticleDocument;
import com.ub.news_market_core.view.search.SearchBlogRequest;
import com.ub.news_market_core.view.search.SearchBlogResponse;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

public interface ArticleService {
    public List<ArticleDocument> listArticles();
    public void addArticle (ArticleDocument article);
    public void updateArticle (ArticleDocument article);
    public void deleteArticle (ObjectId id);
    public ArticleDocument findArticleById(ObjectId id);
    public SearchBlogResponse findAll(SearchBlogRequest request);
    public Long countArticles();
}
