package com.ub.news_market_core.route;


import com.ub.core.base.routes.BaseRoutes;

public class BlogAdminRoute {

    public static final String ROOT = BaseRoutes.ADMIN + "/blog";

    public static final String ADD = ROOT + "/add";
    public static final String EDIT = ROOT + "/edit";
    public static final String ALL = ROOT + "/all";
    public static final String REMOVE = ROOT + "/remove";
}

