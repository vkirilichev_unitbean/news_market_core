package com.ub.news_market_core.route;

public class BlogRoute {

    public static final String ROOT = "/";
    public static final String ADD = "/add-article";
    public static final String SUCCESS = "/success";
}
