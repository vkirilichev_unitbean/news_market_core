package com.ub.news_market_core.controller;

import com.ub.core.base.utils.RouteUtils;
import com.ub.core.picture.model.PictureDoc;
import com.ub.core.picture.repository.PictureRepository;
import com.ub.news_market_core.model.ArticleDocument;
import com.ub.news_market_core.route.BlogRoute;
import com.ub.news_market_core.service.ArticleService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


@Controller
public class BlogController {

    @Autowired
    private ArticleService articleService;
    @Autowired
    private PictureRepository pictureRepository;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String all(Model model){
        model.addAttribute("result", articleService.listArticles());
        model.addAttribute("search", articleService.countArticles());
        return "com.ub.news_market_core.blog.index";
    }

    @RequestMapping(value = BlogRoute.SUCCESS, method = RequestMethod.GET)
    public String success(){
        return "com.ub.news_market_core.blog.success";
    }

    @RequestMapping(value = BlogRoute.ADD, method = RequestMethod.GET)
    public String addArticle(Model model){

        ArticleDocument articleDocument = new ArticleDocument();
        articleDocument.setId(new ObjectId());
        articleDocument.setPicId(new ObjectId());
        model.addAttribute("doc", articleDocument);

        return "com.ub.news_market_core.blog.add-article";
    }

    @RequestMapping(value = BlogRoute.ADD, method = RequestMethod.POST)
    public String addArticle(@ModelAttribute ArticleDocument articleDocument,
                      @RequestParam(required =  false) MultipartFile pic){

        PictureDoc pictureDoc = pictureRepository.save(articleDocument.getPicId(), pic);
        articleDocument.setPicId(pictureDoc.getId());
        articleService.addArticle(articleDocument);

        return RouteUtils.redirectTo(BlogRoute.SUCCESS);
    }
}


