package com.ub.news_market_core.controller;

import com.ub.core.base.utils.RouteUtils;
import com.ub.core.picture.model.PictureDoc;
import com.ub.core.picture.repository.PictureRepository;
import com.ub.core.user.role.AdminUserRole;
import com.ub.news_market_core.model.ArticleDocument;
import com.ub.news_market_core.route.BlogAdminRoute;
import com.ub.news_market_core.service.ArticleService;
import com.ub.news_market_core.view.search.SearchBlogRequest;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.mail.Multipart;

@Controller
public class BlogAdminController {

    @Autowired
    private ArticleService articleService;
    @Autowired
    private PictureRepository pictureRepository;

    @RequestMapping(value = BlogAdminRoute.ADD, method = RequestMethod.GET)
    public String add(Model model){

        ArticleDocument articleDocument = new ArticleDocument();
        articleDocument.setId(new ObjectId());
        articleDocument.setPicId(new ObjectId());
        model.addAttribute("doc", articleDocument);

        return "com.ub.news_market_core.blog.admin.add";
    }

    @RequestMapping(value = BlogAdminRoute.ADD, method = RequestMethod.POST)
    public String add(@ModelAttribute ArticleDocument articleDocument,
                      @RequestParam(required = false) MultipartFile pic, RedirectAttributes ra){

        PictureDoc pictureDoc = pictureRepository.save(articleDocument.getPicId(), pic);
        articleDocument.setPicId(pictureDoc.getId());
        articleService.addArticle(articleDocument);
        ra.addAttribute("id", articleDocument.getId().toString());

        return RouteUtils.redirectTo(BlogAdminRoute.EDIT);
    }

    @RequestMapping (value = BlogAdminRoute.EDIT, method = RequestMethod.GET)
    public String edit(@RequestParam ObjectId id, Model model){

        ArticleDocument articleDocument = articleService.findArticleById(id);
        model.addAttribute("doc", articleDocument);

        return "com.ub.news_market_core.blog.admin.edit";
    }

    @RequestMapping (value = BlogAdminRoute.EDIT, method = RequestMethod.POST)
    public String edit(@ModelAttribute ArticleDocument articleDocument,
                       @RequestParam MultipartFile pic, RedirectAttributes ra){

        PictureDoc pictureDoc = pictureRepository.save(articleDocument.getPicId(), pic);
        articleDocument.setPicId(pictureDoc.getId());
        articleService.updateArticle(articleDocument);
        ra.addAttribute("id", articleDocument.getId());

        return RouteUtils.redirectTo(BlogAdminRoute.EDIT);
    }

    @RequestMapping(value = BlogAdminRoute.ALL, method = RequestMethod.GET)
    public String all(@ModelAttribute SearchBlogRequest request, Model model){
        model.addAttribute("search", articleService.findAll(request));

        return "com.ub.news_market_core.blog.admin.all";
    }

    @RequestMapping(value = BlogAdminRoute.REMOVE, method = RequestMethod.GET)
    public String remove(@RequestParam ObjectId id, Model model){
        model.addAttribute("id", id);
        return "com.ub.news_market_core.blog.admin.remove";
    }

    @RequestMapping(value = BlogAdminRoute.REMOVE, method = RequestMethod.POST)
    public String remove (@RequestParam ObjectId id){
        articleService.deleteArticle(id);
        return RouteUtils.redirectTo(BlogAdminRoute.ALL);
    }
}
