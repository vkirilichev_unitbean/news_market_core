package com.ub.news_market_core.model;

import com.ub.core.base.model.BaseModel;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class ArticleDocument extends BaseModel {

    @Id
    private ObjectId id;
    private String articleTitle;
    private String articleDefinition;
    private ObjectId picId;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getArticleTitle() {
        return articleTitle;
    }

    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle;
    }

    public String getArticleDefinition() {
        return articleDefinition;
    }

    public void setArticleDefinition(String articleDefinition) {
        this.articleDefinition = articleDefinition;
    }

    public ObjectId getPicId() {
        return picId;
    }

    public void setPicId(ObjectId picId) {
        this.picId = picId;
    }

}
