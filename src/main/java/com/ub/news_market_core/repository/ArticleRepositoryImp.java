package com.ub.news_market_core.repository;

import com.ub.news_market_core.model.ArticleDocument;
import com.ub.news_market_core.view.search.SearchBlogRequest;
import com.ub.news_market_core.view.search.SearchBlogResponse;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class ArticleRepositoryImp implements ArticleRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    private static final String COLLECTION_NAME = "articleDocument";

    @Override
    public List<ArticleDocument> getArticleList() {
        return mongoTemplate.findAll(ArticleDocument.class, COLLECTION_NAME);
    }

    @Override
    public void addArticle(ArticleDocument article) {
        mongoTemplate.insert(article, COLLECTION_NAME);
    }

    @Override
    public void updateArticle(ArticleDocument article) {
        mongoTemplate.save(article);
    }

    @Override
    public void deleteArticle(ObjectId id) {
        ArticleDocument articleDocument = this.findArticleById(id);
        mongoTemplate.remove(articleDocument);
    }

    @Override
    public ArticleDocument findArticleById(ObjectId id) {
        return mongoTemplate.findById(id, ArticleDocument.class);
    }

    @Override
    public SearchBlogResponse findAll(SearchBlogRequest request){
        Sort sort = new Sort(Sort.Direction.DESC, "id");

        Criteria criteria  = new Criteria();
        criteria.orOperator(
                Criteria.where("articleTitle").regex(request.getQuery(), "i"),
                Criteria.where("articleDefinition").regex(request.getQuery(), "i")
        );

        Query query = new Query(criteria);
        Long count = mongoTemplate.count(query, ArticleDocument.class);

        query.limit(request.getPageSize());
        query.skip(request.getCurrentPage()*request.getPageSize());
        query.with(sort);

        List<ArticleDocument> articleDocuments = mongoTemplate.find(query, ArticleDocument.class);


        SearchBlogResponse searchBlogResponse = new SearchBlogResponse();
        searchBlogResponse.setAll(count);
        searchBlogResponse.setCurrentPage(request.getCurrentPage());
        searchBlogResponse.setQuery(request.getQuery());
        searchBlogResponse.setPageSize(request.getPageSize());
        searchBlogResponse.setResult(articleDocuments);

        return searchBlogResponse;
    }

    public Long countArticles(){

        Criteria criteria  = new Criteria();
        criteria.where("id").is(ArticleDocument.class);

        Query query = new Query(criteria);

        return mongoTemplate.count(query, ArticleDocument.class);
    }
}
