package com.ub.news_market_core.menu;

import com.ub.core.base.menu.BaseMenu;
import com.ub.core.base.menu.MenuIcons;
import com.ub.news_market_core.route.BlogAdminRoute;

public class BlogAddMenu extends BaseMenu {
    public BlogAddMenu(){
        this.name = "Добавить";
        this.icon = MenuIcons.MDI_ACTION_DESCRIPTION;
        this.parent = new BlogMenu();
        this.url = BlogAdminRoute.ADD;
    }
}
