package com.ub.news_market_core.menu;

import com.ub.core.base.menu.BaseMenu;
import com.ub.core.base.menu.MenuIcons;

public class BlogMenu extends BaseMenu {
    public BlogMenu(){
        this.name = "Блог";
        this.icon = MenuIcons.MDI_ACTION_DESCRIPTION;
    }
}
