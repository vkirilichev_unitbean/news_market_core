<%@ page import="com.ub.news_market_core.route.BlogRoute" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Магазин новостей</title>

    <link rel="stylesheet" href="/static/news_market/css/style.css">
    <link rel="stylesheet" href="/static/news_market/css/fonts.css">
</head>
<body>

    <div class="head">
        <div class='head-logo'></div>
        <div class="head-count">
            Статей, ${search}
        </div>
        <a href="<%=BlogRoute.ADD%>" class="head-blog-create"></a>
    </div>

    <div class="content">
        <c:forEach items="${result}" var="doc">
            <div class="blog-item">
                <div class="blog-item-background" style="background-image: url('/pics/${doc.picId}')">

                </div>

                <div class="blog-item-title">
                    <h4>${doc.articleTitle}</h4>
                </div>

                <div class="blog-item-date">
                    <fmt:formatDate type="date" value="${doc.createdAt}" pattern="yyyy-MM-dd" />
                </div>
                <div class="blog-item-line">

                </div>
                <div class="blog-item-text">
                    ${doc.articleDefinition}
                </div>
            </div>
        </c:forEach>
    </div>

</body>
</html>
