<%@ page import="com.ub.news_market_core.route.BlogAdminRoute" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="section">
    <form:form action="<%= BlogAdminRoute.EDIT %>" class="card-panel" modelAttribute="doc" method="post" enctype="multipart/form-data">
        <h4 class="header2"><s:message code="ubcore.admin.form.adding"/></h4>

        <c:set value="${doc}" var="doc" scope="request"/>

        <jsp:include page="/WEB-INF/com/ub/core/admin/components/errorAlert.jsp"/>

        <jsp:include page="form.jsp"/>

        <div class="row">
            <div class="input-field col s12">
                <form:button class="btn cyan waves-effect waves-light right" type="submit" name="action">
                    <s:message code="ubcore.admin.form.submit"/><i class="mdi-content-send right"></i>
                </form:button>
            </div>
        </div>
    </form:form>
</div>