<%@ page import="com.ub.core.authorization.routes.AuthorizationAdminRoutes" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel">
        <form action="<%=AuthorizationAdminRoutes.INIT%>" class="login-form" method="post">
            <div class="margin">
                <div class="input-field col s12 center">
                    <img src="<c:url value="/static/ub/images/register-logo.png"/>" alt="UnitBean Logo"
                         class="circle responsive-img valign profile-image-login">
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12 center">
                    <h4>Регистрация</h4>
                    <p class="center">Псс... Парень, новый проект, да?</p>
                </div>
            </div>
            <div class="row margin">
                <div class="input-field col s12">
                    <i class="mdi-communication-email prefix"></i>
                    <input id="email" type="email" name="email">
                    <label for="email" class="center-align">Email</label>
                </div>
            </div>
            <div class="row margin">
                <div class="input-field col s12">
                    <i class="mdi-action-lock-outline prefix"></i>
                    <input id="password" type="password" name="password">
                    <label for="password">Password</label>
                </div>
            </div>
            <div class="row margin">
                <div class="input-field col s12">
                    <i class="mdi-action-lock-outline prefix"></i>
                    <input id="secret-password" type="password" name="secretPassword" maxlength="6" minlength="6">
                    <label for="secret-password">Secret Password</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <button class="btn red waves-effect waves-light col s12">Register</button>
                </div>
            </div>
        </form>
    </div>
</div>
