<%@ page import="com.ub.core.file.routes.FileAdminRoutes" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="section">
    <form action="<%= FileAdminRoutes.ADD %>" class="card-panel" method="post" autocomplete="off"
          enctype="multipart/form-data">
        <h4 class="header2"><s:message code="ubcore.admin.form.adding"/></h4>

        <div class="row">
            <div class="col s12">
                <input type="file" name="file" class="js-dropify" data-default-file=""/>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <button class="btn cyan waves-effect waves-light right" type="submit" name="action">
                    <s:message code="ubcore.admin.form.submit"/><i class="mdi-content-send right"></i>
                </button>
            </div>
        </div>
    </form>
</div>