<%@ page import="com.ub.core.social.facebook.routes.FacebookAdminRoutes" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="section">
    <form:form action="<%= FacebookAdminRoutes.SETTINGS_EDIT %>" class="card-panel" modelAttribute="doc" method="post"
               autocomplete="off">
        <h4 class="header2">Изменение настроек Facebook</h4>

        <jsp:include page="/WEB-INF/com/ub/core/admin/components/errorAlert.jsp"/>

        <form:hidden path="id"/>

        <div class="row">
            <div class="input-field col s12">
                <label for="APP_ID">APP_ID</label>
                <form:input path="APP_ID" id="APP_ID"/>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <label for="PERMISSIONS">PERMISSIONS</label>
                <form:input path="PERMISSIONS" id="PERMISSIONS"/>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <label for="REDIRECT_URI">REDIRECT_URI</label>
                <form:input path="REDIRECT_URI" id="REDIRECT_URI"/>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <label for="response_type">response_type</label>
                <form:input path="response_type" id="response_type"/>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <label for="APP_SECRET">APP_SECRET</label>
                <form:input path="APP_SECRET" id="APP_SECRET"/>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <form:button class="btn cyan waves-effect waves-light right" type="submit" name="action">
                    <s:message code="ubcore.admin.form.submit"/><i class="mdi-content-send right"></i>
                </form:button>
            </div>
        </div>
    </form:form>
</div>