<%@ page import="com.ub.core.social.twitter.routes.TwitterAdminRoutes" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="section">
    <form:form action="<%= TwitterAdminRoutes.SETTINGS_EDIT %>" class="card-panel" modelAttribute="doc" method="post"
               autocomplete="off">
        <h4 class="header2">Изменение настроек Twitter</h4>

        <jsp:include page="/WEB-INF/com/ub/core/admin/components/errorAlert.jsp"/>

        <form:hidden path="id"/>


        <div class="row">
            <div class="input-field col s12">
                <label for="yourApiKey">your api key</label>
                <form:input path="yourApiKey" id="yourApiKey"/>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <label for="yourApiSecret">your api secret</label>
                <form:input type="password" path="yourApiSecret" id="yourApiSecret"/>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <form:button class="btn cyan waves-effect waves-light right" type="submit" name="action">
                    <s:message code="ubcore.admin.form.submit"/><i class="mdi-content-send right"></i>
                </form:button>
            </div>
        </div>
    </form:form>
</div>