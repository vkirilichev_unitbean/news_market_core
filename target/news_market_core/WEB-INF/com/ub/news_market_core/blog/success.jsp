<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 18.10.2017
  Time: 10:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Успешное добавление</title>

    <link rel="stylesheet" href="/static/news_market/css/style.css">
    <link rel="stylesheet" href="/static/news_market/css/fonts.css">
</head>
<body>
    <div class="container">
        <div class="blog-success">
            <img class="blog-success-logo" src="/static/news_market/images/success-logo.png" alt=""/>
            <div class="blog-success-title">
                Поздравляем Вас с успешным добавлением статьи в блог UnitBean!
            </div>
        </div>
    </div>
</body>
</html>
