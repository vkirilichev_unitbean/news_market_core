<%@ page import="com.ub.news_market_core.route.BlogRoute" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Магазин новостей</title>

    <link rel="stylesheet" href="/static/news_market/css/style.css">
    <link rel="stylesheet" href="/static/news_market/css/fonts.css">
</head>
<body>
	<div class="content mod-create">
		<div class="blog-create">
            <h1 class="blog-create-title">Добавление статьи</h1>

            <form:form action="<%=BlogRoute.ADD%>" modelAttribute="doc" method="post" enctype="multipart/form-data">

                <form:hidden path="id"/>
                <form:hidden path="picId"/>

                <div class="blog-create-group">
                    <label for="inputHeader" class="blog-create-label">Заголовок</label>
                    <form:input path="articleTitle" id="inputHeader" class="blog-create-input" placeholder="Заголовок статьи"/>
                </div>

                <div class="blog-create-group">
                    <label for="inputDefinition" class="blog-create-label">Описание</label>
                    <form:textarea path="articleDefinition" class="blog-create-input mod-textarea" id="inputDefinition"></form:textarea>
                </div>

                <div class="blog-create-footer">
                    <label class="blog-create-label" for="inputFile">Добавить изображение</label>
                    <div class="blog-create-file">
                        <input type="file" name="pic" class="blog-create-file-input" id="inputFile"/>
                    </div>
                    
                    <form:button type="submit" name="action" class="btn btn-approve">Добавить</form:button>
                    <a href="<%=BlogRoute.ROOT%>" class="btn btn-cancel">Отмена</a>
                </div>
            </form:form>
    </div>
</div>
</body>
</html>
