<%@ page import="com.ub.core.authorization.routes.AuthorizationAdminRoutes" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel">
        <form action="<%=AuthorizationAdminRoutes.LOGIN%>" class="login-form" method="post">

            <c:if test="${error}">
                <div id="card-alert" class="card red">
                    <div class="card-content white-text">
                        <p><i class="mdi-alert-error"></i> Your login attempt was not successful, try again.</p>
                    </div>
                    <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </c:if>

            <div class="row">
                <div class="input-field col s12 center">
                    <img src="<c:url value="/static/ub/images/favicon/150.png"/>" alt="UnitBean Logo"
                         class="circle responsive-img valign profile-image-login">
                    <p class="center login-form-text">UnitBean Admin Panel</p>
                </div>
            </div>
            <div class="row margin">
                <div class="input-field col s12">
                    <i class="mdi-social-person-outline prefix"></i>
                    <input id="username" type="text" name="email">
                    <label for="username" class="center-align">Email</label>
                </div>
            </div>
            <div class="row margin">
                <div class="input-field col s12">
                    <i class="mdi-action-lock-outline prefix"></i>
                    <input id="password" type="password" name="password">
                    <label for="password" class="active">Password</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <button class="btn waves-effect red waves-light col s12">Login</button>
                </div>
            </div>

        </form>
    </div>
</div>