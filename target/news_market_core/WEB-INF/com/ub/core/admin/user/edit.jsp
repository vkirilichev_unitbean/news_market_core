<%@ page import="com.ub.core.user.routes.UserAdminRoutes" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="section">
    <form:form action="<%= UserAdminRoutes.EDIT %>" class="card-panel" modelAttribute="viewModel" method="post" autocomplete="off">
        <h4 class="header2"><s:message code="ubcore.admin.form.editing"/></h4>

        <jsp:include page="/WEB-INF/com/ub/core/admin/components/errorAlert.jsp"/>

        <form:hidden path="id"/>

        <div class="row">
            <div class="input-field col s12">
                <form:input path="phone" id="phone" type="tel"/>
                <label for="phone">Phone</label>
            </div>

            <div class="input-field col s12">
                <form:input path="login" id="login" type="text"/>
                <label for="login">Login</label>
            </div>
        </div>

        <jsp:include page="form.jsp"/>

        <div class="row">
            <div class="input-field col s12">
                <form:password path="password" id="new-password"/>
                <label for="new-password">New password</label>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <form:button class="btn cyan waves-effect waves-light right" type="submit" name="action">
                    <s:message code="ubcore.admin.form.submit"/><i class="mdi-content-send right"></i>
                </form:button>
            </div>
        </div>
    </form:form>
</div>

<%--
<div class="tab-pane" id="logs">
    <h3><i class="fa fa-tasks" aria-hidden="true"></i> История входов пользователя </h3>
    <table class="table table-bordered" id="table-1">
        <thead>
        <tr>
            <th>№</th>
            <th>Дата</th>
            <th>IP</th>
            <th>Статус</th>
            <th>Бразуер</th>
            <th>Операционная система</th>
            <th>Тип устройства</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${userLogs}" var="log" varStatus="status">

            <tr>
                <td>${status.index + 1}</td>
                <td>${log.createdAt}</td>
                <td>${log.ip}</td>
                <td>${log.status.title}</td>
                <td>${log.browser}(${log.browserVersion})</td>
                <td>${log.operatingSystem}</td>
                <td>${log.deviceType}</td>

            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>--%>