<%@ page import="com.ub.core.user.role.UserRole" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--@elvariable id="viewModel" type="com.ub.core.user.view.UserAddViewModel"--%>

<div class="row">
    <div class="input-field col s12">
        <form:input path="email" id="email" type="email" required="true"/>
        <label for="email">Email</label>
    </div>
</div>

<div class="row">
    <div class="input-field col s12 l6">
        <form:input path="firstName" id="firstName" type="text"/>
        <label for="firstName">First Name</label>
    </div>
    <div class="input-field col s12 l6">
        <form:input path="lastName" id="lastName" type="text"/>
        <label for="lastName">Last Name</label>
    </div>
</div>

<div class="row">
    <div class="input-field col s12">
        <select name="roleNames" id="roleNames" multiple>
            <option value="" disabled>Choose any roles</option>
            <c:forEach items="<%= UserRole.findAll() %>" var="entry">
                <option value="${entry.key}"
                    ${viewModel.roleNames.contains(entry.key) ? 'selected' : ''}>${entry.value.title}</option>
            </c:forEach>
        </select>
        <label for="roleNames">Roles</label>
    </div>
</div>