<%@ page import="com.ub.core.pictureTask.routes.PictureTaskAdminRoutes" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--@elvariable id="search" type="com.ub.core.pictureTask.view.all.SearchPictureTaskAdminViewModel"--%>

<div class="section">
    <table class="responsive-table bordered">
        <thead>
        <tr>
            <th>Picture ID</th>
            <th>Size type</th>
            <th>Status</th>
            <th><s:message code="ubcore.admin.createdAt"/></th>
            <th><s:message code="ubcore.admin.updateAt"/></th>

            <th><s:message code="ubcore.admin.action"/></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${search.result}" var="doc" varStatus="status">
            <tr>
                <td>${doc.pictureId}</td>
                <td>${doc.pictureSizeType}</td>
                <td>${doc.status}</td>
                <td><fmt:formatDate type="both" dateStyle="short" timeStyle="short"
                                    value="${doc.createdAt}"/></td>
                <td><fmt:formatDate type="both" dateStyle="short" timeStyle="short"
                                    value="${doc.updateAt}"/></td>
                <td>
                    <c:url value="<%=PictureTaskAdminRoutes.EDIT%>" var="editUrl">
                        <c:param name="id" value="${doc.id}"/>
                    </c:url>
                    <a href="${editUrl}" class="btn btn-small waves-effect waves-light blue">
                        <s:message code="ubcore.admin.action.edit"/><i class="mdi-editor-mode-edit left"></i>
                    </a>
                    <c:url value="<%= PictureTaskAdminRoutes.REMOVE %>" var="removeUrl">
                        <c:param name="id" value="${doc.id}"/>
                    </c:url>
                    <a href="${removeUrl}" class="btn btn-small waves-effect waves-light red js-link-remove-doc">
                        <s:message code="ubcore.admin.action.remove"/><i class="mdi-action-delete left"></i>
                    </a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

<div class="section">
    <div class="row center-align">
        <ul class="pagination">
            <c:url value="<%=PictureTaskAdminRoutes.ALL%>" var="prevUrl">
                <c:param name="query" value="${search.query}"/>
                <c:param name="currentPage" value="${search.prevNum}"/>
            </c:url>
            <li class="${search.prevNum eq search.currentPage ? 'disabled' : 'waves-effect'}">
                <a <c:if test="${search.prevNum ne search.currentPage}">href="${prevUrl}"</c:if>>
                    <i class="mdi-navigation-chevron-left"></i>
                </a>
            </li>

            <c:forEach items="${search.pagination}" var="page">
                <c:url value="<%=PictureTaskAdminRoutes.ALL%>" var="pageUrl">
                    <c:param name="query" value="${search.query}"/>
                    <c:param name="currentPage" value="${page}"/>
                </c:url>
                <li class="${search.currentPage eq page ? 'active' : ''}">
                    <a href="${search.currentPage ne page ? pageUrl : ''}">${page + 1}</a>
                </li>
            </c:forEach>

            <c:url value="<%=PictureTaskAdminRoutes.ALL%>" var="nextUrl">
                <c:param name="query" value="${search.query}"/>
                <c:param name="currentPage" value="${search.nextNum}"/>
            </c:url>
            <li class="${search.nextNum eq search.currentPage ? 'disabled' : 'waves-effect'}">
                <a <c:if test="${search.nextNum ne search.currentPage}">href="${nextUrl}"</c:if>>
                    <i class="mdi-navigation-chevron-right"></i>
                </a>
            </li>
        </ul>
    </div>
</div>