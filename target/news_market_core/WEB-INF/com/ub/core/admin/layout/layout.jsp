<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--@elvariable id="pageHeader" type="com.ub.core.base.view.PageHeader"--%>

<%-- Request variables --%>
<s:eval var="currentUser" expression="@authorizationService.optionalUser" scope="request"/>
<c:set var="requestURL" value="${requestScope['javax.servlet.forward.request_uri']}" scope="request"/>
<c:set var="language" value="${pageContext.response.locale.language}" scope="request"/>

<!DOCTYPE html>
<html lang="${language}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">

    <jsp:include page="components/seoTags.jsp"/>

    <title>${not empty pageHeader ? pageHeader.breadcrumbs.currentTitle : "Admin Panel" } | UBCore</title>

    <jsp:include page="components/styles.jsp"/>
    <tiles:insertAttribute name="head"/>
</head>
<body>
<%--<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>--%>

<jsp:include page="components/header.jsp"/>

<main id="main">
    <div class="wrapper">
        <jsp:include page="components/sidebar.jsp"/>

        <section id="content">

            <jsp:include page="components/breadcrumbs.jsp"/>

            <div class="container">
                <tiles:insertAttribute name="content"/>
            </div>
        </section>
    </div>
</main>

<jsp:include page="components/footer.jsp"/>

<jsp:include page="components/scripts.jsp"/>

<tiles:importAttribute name="modals"/>
<c:forEach items="${modals}" var="modalItem">
    <tiles:insertAttribute value="${modalItem}" flush="true"/>
</c:forEach>

</body>
</html>